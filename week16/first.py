
#!/usr/bin/env python3
import time
import sys
with open(sys.argv[1]) as f:
    my_list = [line.rstrip('\n') for line in f]
    start = time.time()
    def bubbleSort(my_list):
        swapped = True
        while swapped:
            swapped = False
            for i in range(len(my_list) - 1):
                if my_list[i] < my_list[i + 1]:
                    my_list[i], my_list[i + 1] = my_list[i + 1], my_list[i]
                    swapped = True
        return my_list

    bubbleSort(my_list)
    print("-Sorted in Descending Order with Bubble Sort-")
    for i in range(len(my_list)):
        print(" %s " % my_list[i])

    end = time.time()
    t = end-start
    print(t)

#BINARY  SORT
    start1 = time.time()
    def BinarySort(my_list):
        for i in range(1, len(my_list)):

            key = my_list[i]

            j = i - 1
            while j >= 0 and key > my_list[j]:
                my_list[j + 1] = my_list[j]
                j -= 1
                my_list[j + 1] = key
    BinarySort(my_list)
    print("-Sorted in Descending Order with Binary Sort-")
    for i in range(len(my_list)):
        print("%s" % my_list[i])
    end1 = time.time()
    t1 = end1 - start1
    print(t1)
