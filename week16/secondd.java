
import java.io.*;
import java.util.ArrayList;
public class second {

	@SuppressWarnings("resource")
	public static void main(String[] args)throws Exception {
	//Reading a text file and storing values in arraylist
		BufferedReader in = new BufferedReader(new FileReader(args[0]));
		String st=null;
		ArrayList<String> lines = new ArrayList<String>();
		while((st = in.readLine()) != null){
		    lines.add(st);
		}
		String[] linesArray = lines.toArray(new String[lines.size()]);
		  System.out.println("Unsorted list" + lines);
		  System.out.println("-Sorted in Descending Order with Bubble Sort-");
		long startTime = System.nanoTime();
		String key;
		  for (int j = 0; j < linesArray.length; j++) {
		         for (int i = j + 1; i < linesArray.length; i++) {
		            // comparing strings
		            if (linesArray[i].compareTo(linesArray[j]) > 0) {
		               key = linesArray[j];
		               linesArray[j] = linesArray[i];
		               linesArray[i] = key;
		            }

		         }
		        System.out.println( linesArray[j]);
		  }
			long endTime = System.nanoTime();
        System.out.println("Sorted in " + (endTime - startTime) + " nano seconds");

					 		 System.out.println("-Sorted in Descending Order with Binary Sort-");
							 long startTime2 = System.nanoTime();
							 for (int j = 0; j < linesArray.length; j++) {


							 key = linesArray[j];
				     	 int	i = j - 1;
				     			while (i >= 0) {
				       			if (key.compareTo(linesArray[i]) < 0) {

										break;
				       			}
				       		linesArray[i + 1] = linesArray[i];
				       		i-=1;

				     	linesArray[i + 1] = key;
						 }
						 System.out.println(linesArray[j]);


					}
					long endTime2 = System.nanoTime();
					System.out.println("Sorted in " + (endTime2 - startTime2) + " nano seconds");
				}
			}
